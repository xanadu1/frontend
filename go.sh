#!/bin/sh

wasm-pack build --dev --out-dir www/wasm --target bundler
cd www && npm install && npm run build

/usr/local/bin/miniserve -v --index index.html dist
