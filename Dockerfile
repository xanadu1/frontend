######################################################################
# This Dockerfile produces a small Docker image containing static web
# content for delivering CSS, JavaScript and HTML, and serves it via a
# file-oriented webserver on port 8080.  This web content represents a
# dynamic website written in Rust compiled into WebAssembly and bound
# to JavaScript which is loaded by a little HTML.
######################################################################

########################################
# Build Stage
FROM rust:1.52.1-slim-buster as builder

ARG PROFILE="--dev"
### ARG PROFILE="--release"

# Install the necessary DEVELOPMENT tools for building things.
#
# INSTALL the package 'software-properties-common' to abstract the apt
# repositories used, and then install the 'npm' package manager so
# that we can more easily install Node.js packages with automatic
# dependency handling versus manual wget/unzip/cp steps.  Installing
# 'npm' brings along with it Node.js, a JavaScript runtime built on
# Chrome's V8 JavaScript engine.
#
RUN apt-get update              \
 && apt-get install -y          \
    software-properties-common  \
    curl                        \
 && rm -rf /var/lib/apt/lists/*

# INSTALL a -specific- version of node.js and npm, as the one packaged in the
# base Docker image is old.
#
ENV NODE_VERSION 14.17.0
RUN curl -fsSLO --compressed "https://nodejs.org/dist/v$NODE_VERSION/node-v$NODE_VERSION-linux-x64.tar.xz"  \
 && tar -xJf "node-v$NODE_VERSION-linux-x64.tar.xz" -C /usr/local --strip-components=1 --no-same-owner      \
 && rm -f "node-v$NODE_VERSION-linux-x64.tar.xz"

# INSTALL the 'webpack' bundler, that packages up JavaScript, CSS and
# WebAssembly into the fewest number of files and minifies source where
# possible.
#
RUN npm install --global   \
      webpack              \
      webpack-cli

# INSTALL the 'wasm-pack' tool, a one-stop shop for orchestrating the
# compilation of our Rust modules into a binary WebAssembly module and
# packaging them with metadata into the 'npm' format suitable for
# publishing into the npm registry or using alongside other JavaScript
# packages such as 'webpack'
#
RUN curl https://rustwasm.github.io/wasm-pack/installer/init.sh -sSf | sh

# PULL executable for a file-oriented webserver written in Rust, for serving files.
# We'll install it into the deployment Docker image later.
#
ARG MINISERVE_VER="v0.14.0"
ARG MINISERVE_PKG="miniserve-${MINISERVE_VER}-x86_64-unknown-linux-musl"
ARG MINISERVE_URL="https://github.com/svenstaro/miniserve/releases/download/${MINISERVE_VER}"
RUN curl -fsSLO --compressed ${MINISERVE_URL}/${MINISERVE_PKG}       \
 && mv                    ${MINISERVE_PKG} /usr/local/bin/miniserve  \
 && chmod +x /usr/local/bin/miniserve

# First we establish a /buildarea under which all artifacts will be
# constructed.  We start with the topmost directory containing a Rust
# project with a Cargo.toml control file.
#
# This directory we COPY in was initially created using the following
# command, and then modified by the developer to implement the desired
# web application.
#
#    $ wasm-pack new frontend --mode normal
#
# frontend/
# ├── Cargo.toml
# ├── LICENSE_APACHE
# ├── LICENSE_MIT
# ├── README.md
# ├── src
# │   ├── lib.rs
# │   └── utils.rs
# ├── www
# │   ├── index.html         - root HTML file, loads bootstrap.js
# │   ├── bootstrap.js       - asynchronously imports index.js and sub-modules used
# │   ├── index.js           - main entrypoint for the SPA's JavaScript
# │   ├── LICENSE-APACHE
# │   ├── LICENSE-MIT
# │   ├── package.json       - metadata for this JavaScript package
# │   ├── README.md
# │   └── webpack.config.js  - config file for webpack and its local dev server
# └── tests
#     └── web.rs

ENV  HOME=/buildarea
RUN mkdir -p ${HOME}
WORKDIR ${HOME}

COPY . .

# This directory we COPY in was initially created by the developer using this
# command:
#
#    $ wasm-pack new frontend --mode normal
#
# followed by this command to create the subdirectory 'www':
#
#    $ npm init wasm-app www
#
# and whatever necessary of these commands to install the JavaScript packages
# we need to operate and build our application.  These packages are retained
# under the www/node_packages/ subdirectory and copied into the Docker image
# with the "COPY . ." command above.
#
#    $ cd www && npm install --save copy-webpack-plugin@5.1.1
#
# And then all that was modified by the developer to implement the desired web
# application.
#
# frontend/
# ├── Cargo.toml
# ├── LICENSE_APACHE
# ├── LICENSE_MIT
# ├── README.md
# ├── src/
# │   ├── lib.rs
# │   └── utils.rs
# ├── www/
# │   ├── index.html         - root HTML file, loads bootstrap.js
# │   ├── bootstrap.js       - asynchronously imports index.js and sub-modules used
# │   ├── index.js           - main entrypoint for the SPA's JavaScript
# │   ├── LICENSE-APACHE
# │   ├── LICENSE-MIT
# │   ├── package.json       - metadata for this JavaScript package
# │   ├── README.md
# │   └── webpack.config.js  - config file for webpack and its local dev server
# └── tests/
#     └── web.rs

# We are now ready to compile our Rust source, in the top directory, into a
# binary WebAssembly (.wasm) module and package it into a JavaScript package
# underneath the www/ hierarchy that is suitable for interoperation with a
# bundler like WebPack.

RUN wasm-pack build ${PROFILE} --out-dir www/wasm --target bundler

# buildarea/
# ├── ...
# ├── www/
# │   ├── ...
# │   ├── wasm/
# │   │   ├── README.md                       -- copied from top-level directory
# │   │   ├── package.json                    -- metadata about bindings, used by npm and JavaScript bundlers
# │   │   ├── wasm_game_of_life.js            -- generated by wasm-bindgen to import JavaScript items
# │   │   │                                      into Rust, and expose Rust items to JavaScript
# │   │   ├── wasm_game_of_life_bg.wasm       -- our WebAssembly binary generated by the Rust compiler
# │   │   ├── wasm_game_of_life_bg.js         -- ???
# │   │   ├── wasm_game_of_life.d.ts          -- generated TypeScript type declarations
# │   │   └── wasm_game_of_life_bg.wasm.d.ts  -- ???

# Next we change into the www/ subdirectory and run the WebPack
# builder, automatically configured into a script named 'build'
# declared inside the package.json file.  It uses the file
# www/webpack.config.js to drive this process.

RUN cd www        \
 && npm install   \
 && npm run build

# The WebPack builder creates a new subdirectory under www/ that receives the
# binary artifacts representing your packaged webapp, ready for deployment by
# a static webserver.

# frontend/
# ├── ...
# ├── www/
# │   ├── ...
# │   ├── wasm/
# │   │   ├── ...
# │   ├── dist/
# │   │   ├── 0.bootstrap.js
# │   │   ├── bootstrap.js
# │   │   ├── d6748bceec1cdb279285.module.wasm
# │   │   ├── e8d12bf231e16273747c.module.wasm
# │   │   ├── index.html
# │   │   └── favicon.ico

# The final buildarea/ tree with everything in it looks like:
#
# frontend/
# ├── Cargo.toml                              -- manifest file for Rust package
# ├── LICENSE_APACHE
# ├── LICENSE_MIT
# ├── README.md
# ├── src/
# │   ├── lib.rs
# │   └── utils.rs
# ├── www/
# │   ├── package.json                        -- metadata for this root-level JavaScript package
# │   ├── index.html                          -- root HTML file, loads bootstrap.js
# │   ├── bootstrap.js                        -- asynchronously imports index.js and sub-modules used
# │   ├── index.js                            -- main entrypoint for the SPA's JavaScript
# │   ├── favicon.ico                         -- site icon
# │   ├── LICENSE-APACHE
# │   ├── LICENSE-MIT
# │   ├── README.md
# │   ├── webpack.config.js                     -- config file for webpack and its local dev server
# │   ├── wasm/
# │   │   ├── package.json                    -- metadata about bindings, used by npm and JavaScript bundlers
# │   │   ├── frontend_bg.wasm                -- our WebAssembly binary generated by the Rust compiler
# │   │   ├── frontend.js                     -- generated by wasm-bindgen to import JavaScript items
# │   │   │                                      into Rust, and expose Rust items to JavaScript
# │   │   ├── frontend.d.ts                   -- generated TypeScript type declarations
# │   │   ├── frontend_bg.js                  -- ???
# │   │   ├── frontend_bg.wasm.d.ts           -- ???
# │   │   └── README.md                       -- copied from project
# │   └── dist/
# │       ├── 0.bootstrap.js
# │       ├── bootstrap.js
# │       ├── d6748bceec1cdb279285.module.wasm  -- ??? why two ???
# │       ├── e8d12bf231e16273747c.module.wasm
# │       ├── index.html
# │       └── favicon.ico                       -- site icon, copied from www/favicon.ico
# └── tests/
#     └── web.rs

########################################
# Deploy Stage
FROM debian:buster-slim

ENV  HOME=/web
RUN mkdir -p ${HOME}
WORKDIR ${HOME}

# COPY in a basic webserver that just delivers local static files.
COPY --from=builder /usr/local/bin/miniserve /usr/local/bin/miniserve

# COPY the automatically packaged, minified files built for us and
# which are directly served -- what you would upload to your webserver.
COPY --from=builder /buildarea/www/dist/  ${HOME}/

###
# By leaving ENTRYPOINT empty but providing CMD, we allow the default behavior
# when running the Docker container of starting up the server but also to run
# something else manually by invoking the container with /bin/bash to get a
# shell prompt.

ENTRYPOINT []

# Only utilized if NO command is supplied when running the container, this
# defines the "default executable" of a Docker image.
CMD ["/usr/local/bin/miniserve", "-v", "--index", "index.html"]

EXPOSE 8080
