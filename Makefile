# The three things that identify a Docker image in a registry
namespace ?= xanadu1
image := $(namespace)/frontend
registry ?= registry.gitlab.com

MAKEFLAGS += --warn-undefined-variables
.DEFAULT_GOAL := build
.PHONY: *

# we get these from CI environment if available, otherwise from git
GIT_COMMIT ?= $(shell git rev-parse --short HEAD)
GIT_BRANCH ?= $(shell git rev-parse --abbrev-ref HEAD)

tag := branch-$(shell basename $(GIT_BRANCH))-$(GIT_COMMIT)

## Display this help message
help:
	@awk '/^##.*$$/,/[a-zA-Z_-]+:/' $(MAKEFILE_LIST) | awk '!(NR%2){print $$0p}{p=$$0}' | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-20s\033[0m %s\n", $$1, $$2}' | sort

# ------------------------------------------------
# Container builds

# Build the container image locally
build:
	#docker build --target builder -t=builder -f Dockerfile .
	docker build -t=$(image):$(tag) -f Dockerfile \
	  --label git-commit=$(GIT_COMMIT)  \
	  --label git-branch=$(GIT_BRANCH)  \
	  .
	#docker images builder
	docker images $(image):$(tag)

## Log into the Docker Registry (will prompt for username/password)
login:
	docker login $(registry)

## Tag and push the current container image to the Docker Registry
push:
	docker tag  $(image):$(tag) $(registry)/$(image):$(tag)
	docker push                 $(registry)/$(image):$(tag)

## Tag as 'latest' and push the current container image to our Registry
release: push
	docker tag  $(image):$(tag) $(registry)/$(image):latest
	docker push                 $(registry)/$(image):latest

## Pull the container image from the Docker Registry
pull:
	docker pull $(registry)/$(image):$(tag)

## Print environment variables for build debugging
info:
	@echo GIT_COMMIT=$(GIT_COMMIT)
	@echo GIT_BRANCH=$(GIT_BRANCH)
	@echo image=$(image)
	@echo namespace=$(namespace)
	@echo tag=$(tag)
	@echo registry=$(registry)

## Run the container as a webserver assuming it doesn't need other services.
run:
	docker run --interactive --tty --rm  \
	  -p 8080:8080                       \
	  $(image):$(tag)

## Run the container and drop into a shell within it for manual exploration.
shell:
	-docker run --interactive --tty --rm  \
	  -p 8080:8080                        \
	  $(image):$(tag)                     \
	  /bin/bash
